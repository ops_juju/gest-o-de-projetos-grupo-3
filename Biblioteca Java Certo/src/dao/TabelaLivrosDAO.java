package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.ModeloMySql;
import model.ModeloTabelaLivro;
import model.ModeloTabelaSocio;
import controller.FabricaDeConexao;

public class TabelaLivrosDAO {

	/***
	 *CREATE TABLE livro ( codlivro varchar(10), varchar(50),
	 *cpf varchar(15), endereco varchar(100), numeroendereco int,
	 *estadoCivil varchar(10),genero(50));
	 *SHOW TABLES;
	 *DESCRIBE socio;
	 *SELECT * FROM socio;
	 */
	private ModeloMySql modelo = null;
	public TabelaLivrosDAO (){

		modelo = new ModeloMySql();

		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("biblioteca");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}


	/////////////////////// inserir ///////////////////////
	public boolean Inserir(ModeloTabelaLivro livro){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO livro (codLivro,titulo,autor,ano,dataIda,dataVolta,estado) VALUES(?,?,?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, livro.getCodLivro());
			statement.setString(2, livro.getTitulo());
			statement.setString(3, livro.getAutor());
			statement.setInt(4, livro.getAno());
			statement.setString(5, "Hoje");
			statement.setString(6, "Validar");
			statement.setString(7, "Na estante");



			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}
	///////////////////// listar 	///////////////////////////
	public ArrayList<ModeloTabelaLivro> Listar(){

		Connection con = FabricaDeConexao.getConnection(modelo);

		/* vetor para armazenar os livros obtidos */
		ArrayList<ModeloTabelaLivro> livros = new ArrayList<ModeloTabelaLivro>();

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return null;
		}


		/* prepara o sql para selecionar no banco */

		String sql = "SELECT * FROM livro;";

		/* prepara para selecionar no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			/* recebe os resultados da query */
			while(result.next()){
				ModeloTabelaLivro livro = new ModeloTabelaLivro();
				livro.setCodLivro(result.getString("codLivro"));
				livro.setTitulo(result.getString("titulo"));
				livro.setAutor(result.getString("autor"));
				livro.setAno(result.getInt("ano"));
				livro.setDataIda(result.getString("dataIda"));
				livro.setDataVolta(result.getString("dataVolta"));

				/*adiciona novo livro encontrado no vetor de livros*/
				livros.add(livro);
			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return livros;
	}

	///////////////// deletar/////////////////////////
	public boolean Delete(String key){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "DELETE FROM livro where codLivro = '" + key + "';";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}
	/////////////////////////////// alterar ////////////////////////////////
	public boolean Alterar(ModeloTabelaLivro livro, String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}

		String sql = " UPDATE livro SET codLivro=?, titulo=?, autor=?, ano=?,dataIda=?,dataVolta=? WHERE codLivro=?;";

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, livro.getCodLivro());
			statement.setString(2, livro.getTitulo());
			statement.setString(3, livro.getAutor());
			statement.setInt(4, livro.getAno());
			statement.setString(5, livro.getDataIda());
			statement.setString(6, livro.getDataVolta());
			statement.setString(7, key);


			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			//TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			//Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return true;
	}
	////////////////////////Pesquisar //////////////////
	public ArrayList<ModeloTabelaLivro> Pesquisar(){
		Connection con = FabricaDeConexao.getConnection(modelo);
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<ModeloTabelaLivro> livros = new ArrayList<ModeloTabelaLivro>();

		String sql = "SELECT * FROM livro;";


		try {
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();

			while(rs.next()){
				ModeloTabelaLivro livro = new ModeloTabelaLivro();

				livro.setCodLivro(rs.getString("codLivro"));
				livro.setTitulo(rs.getString("titulo"));
				livro.setAutor(rs.getString("autor"));
				livro.setAno(rs.getInt("ano"));
				livro.setDataIda(rs.getString("dataIda"));
				livro.setDataVolta(rs.getString("dataVolta"));
				livros.add(livro);  
			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		}

		return livros;
	}

	///////////////////// Alterar Estado////////////

	public boolean AlterarEstado(ModeloTabelaLivro livro,String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}

		String sql = "UPDATE livro SET "
				+ "codLivro=?,"
				+"dataIda=?,"
				+"dataVolta=?,"
				+"estado=?"
				+"WHERE codLivro=?;";
		
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			/* preenche com os dados */
	
			statement.setString(1, livro.getCodLivro());
			statement.setString(2, livro.getDataIda());
			statement.setString(3, livro.getDataVolta());
			statement.setString(4, livro.getEstado());
			statement.setString(5, key);


			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			//TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			//Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return true;
	}

}
