 package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.ModeloTabelaSocio;

public class TabelaSocioDAO {
  
	/***
	 *CREATE TABLE socio ( codSocio varchar(10),nome varchar(50),
	 *cpf varchar(15), endereco varchar(100), numeroendereco int,
	 *estadoCivil varchar(10),genero(50),estado varchar(15));
	 *SHOW TABLES;
	 *DESCRIBE socio;
	 *SELECT * FROM socio;
	 */
	private ModeloMySql modelo = null;

	public TabelaSocioDAO(){
 
		modelo = new ModeloMySql();

		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("biblioteca");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}

	/////////////////////// inserir ///////////////////////
	public boolean Inserir(ModeloTabelaSocio socio){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO socio (codSocio,nome,cpf,endereco,numeroendereco,estadoCivil,genero,estado) VALUES(?,?,?,?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, socio.getCodSocio());
			statement.setString(2, socio.getNome());
			statement.setString(3, socio.getCpf());
			statement.setString(4, socio.getEndereco());
			statement.setInt(5,socio.getNumeroendereco());
			statement.setString(6, socio.getEstadoCivil());
			statement.setString(7, socio.getGenero());
			statement.setString(8, "Em dia");



			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}
	/////////////////////////// deletar/////////////////////////
    public boolean Delete(String key){
		
		boolean status= true;
		
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}
		
		
		/* prepara o sql para inserir no banco */
		
		String sql = "DELETE FROM socio WHERE codSocio =?;";
		
		/* prepara para inserir no banco de dados */
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
			/* preenche com os dados */
			statement.setString(1, key);
			
			
			/* executa o comando no banco */
			statement.executeUpdate();
	        status = true;
			
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		
		}
		
		return status;
		
	}
		
	/////////////////////////////// alterar ////////////////////////////////
    public boolean Alterar(ModeloTabelaSocio socio, String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}

		String sql = " UPDATE socio SET "
				+ "codSocio=?,"
				+ " nome=?,"
				+ " cpf=?,"
				+ " endereco=?,"
				+ "numeroendereco=?,"
				+ "estadoCivil=?,"
				+ "genero=?"
				+ "estado=?"
				+"WHERE codSocio=?;";

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, socio.getCodSocio());
			statement.setString(2, socio.getNome());
			statement.setString(3, socio.getCpf());
			statement.setString(4, socio.getEndereco());
			statement.setInt(5, socio.getNumeroendereco());
			statement.setString(6, socio.getEstadoCivil());
			statement.setString(7, socio.getGenero());
			statement.setString(8, key);

			 
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			//TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			//Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return true;
	}

	////////////////////////Pesquisar //////////////////
	public ArrayList<ModeloTabelaSocio> Pesquisar(){
		
		Connection con = FabricaDeConexao.getConnection(modelo);
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<ModeloTabelaSocio> socio = new ArrayList<ModeloTabelaSocio>();

		String sql = "SELECT * FROM socio;";


		try {
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();

			while(rs.next()){
				ModeloTabelaSocio socios = new ModeloTabelaSocio();

				socios.setCodSocio(rs.getString("codSocio"));
				socios.setNome(rs.getString("nome"));
				socios.setCpf(rs.getString("cpf"));
				socios.setEndereco(rs.getString("endereco"));
				socios.setNumeroendereco(rs.getInt("numeroendereco"));
				socios.setEstadoCivil(rs.getString("estadoCivel"));
				socios.setGenero(rs.getString("genero"));
				socio.add(socios);}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		}

		return socio;
	}
	 //coment
	
	///////////////////// Alterar Estado////////////
	public boolean AlterarEstado(ModeloTabelaSocio socio, String estado,String key){
		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}

		String sql = " UPDATE socio SET "
				+ "codSocio=?,"
				+ "estado=?"
				+"WHERE codSocio=?;";

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, socio.getCodSocio());
			statement.setString(2, socio.getEstado());
			statement.setString(3, key);

			 
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			//TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			//Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return true;
	}
	}


