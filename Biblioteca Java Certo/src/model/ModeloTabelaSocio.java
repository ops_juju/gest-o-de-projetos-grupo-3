package model;

public class ModeloTabelaSocio {
	private String codSocio;
	private String nome;
	private String cpf;
	private String endereco;
	private int numeroendereco;
	private String estadoCivil;
	private String genero;
	private String estado;
	
	 public ModeloTabelaSocio(String codSocio, String estado) {
		super();
		this.codSocio = codSocio;
		this.estado = estado;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	//coment
	public String getCodSocio() {
		return codSocio;
	}
	public void setCodSocio(String codSocio) {
		this.codSocio = codSocio;
	}
	public String getNome() { 
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public int getNumeroendereco() {
		return numeroendereco;
	}
	public void setNumeroendereco(int numeroendereco) {
		this.numeroendereco = numeroendereco;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public ModeloTabelaSocio(String codSocio, String nome, String cpf,
			String endereco, int numeroendereco, String estadoCivil,
			String genero) {
		super();
		this.codSocio = codSocio;
		this.nome = nome;
		this.cpf = cpf;
		this.endereco = endereco;
		this.numeroendereco = numeroendereco;
		this.estadoCivil = estadoCivil;
		this.genero = genero;
	}
	public ModeloTabelaSocio() {
		super();
		// TODO Auto-generated constructor stub
	}

}
