 package model;

public class ModeloTabelaLivro {
	  //coment
	
	
	private String codLivro;
	private String titulo;
	private String autor;
	private int ano;
	private String dataIda;
	private String dataVolta;
	private String estado;
	

	public ModeloTabelaLivro(String codLivro,String estado,String dataIda,String dataVolta) {
		
		super();
		this.codLivro = codLivro;
		this.estado = estado;
		this.dataIda = dataIda;
		this.dataVolta = dataVolta;
		
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodLivro() {
		return codLivro;
	}
	public void setCodLivro(String codLivro) {
		this.codLivro = codLivro;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public String getDataIda() {
		return dataIda;
	}
	public void setDataIda(String dataIda) {
		this.dataIda = dataIda;
	}
	public String getDataVolta() {
		return dataVolta;
	}
	public void setDataVolta(String dataVolta) {
		this.dataVolta = dataVolta;
	}
	
	public ModeloTabelaLivro() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ModeloTabelaLivro(String codLivro, String titulo, String autor,
			int ano) {
		super();
		this.codLivro = codLivro;
		this.titulo = titulo;
		this.autor = autor;
		this.ano = ano;
	}
	
	public ModeloTabelaLivro(String codLivro, String titulo, String autor,
			int ano, String dataIda, String dataVolta) {
		this.codLivro = codLivro;
		this.titulo = titulo;
		this.autor = autor;
		this.ano = ano;
		this.dataIda = dataIda;
		this.dataVolta = dataVolta;
	}
	

}
