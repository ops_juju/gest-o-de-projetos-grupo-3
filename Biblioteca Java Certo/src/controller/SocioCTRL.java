package controller;

import javax.swing.JOptionPane;





import model.ModeloTabelaSocio;
import dao.TabelaSocioDAO;
//linha de teste straus

public class SocioCTRL {

	/////////////////////// cadastrar ///////////////////////
	public void CadastrarSocio(String codSocio ,String nome, String cpf, String endereco, int numeroendereco,
			String estadoCivil,String genero){
		/* recupera os dados da interface gráfica */

		ModeloTabelaSocio socio = new ModeloTabelaSocio(codSocio ,nome, cpf, endereco, numeroendereco, estadoCivil, genero);

		TabelaSocioDAO daoSocio = new TabelaSocioDAO();
		if(daoSocio.Inserir(socio)){
			JOptionPane.showMessageDialog(null,"Sucesso", "Gravado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao gravar.",JOptionPane.ERROR_MESSAGE);
		}

	}
	///////////////////// alterar/////////////////////////////
	public void AlterarSocio(String codSocio ,String nome, String cpf, String endereco, int numeroendereco,
			String estadoCivil,String genero,String codNovo) {

		ModeloTabelaSocio socio = new ModeloTabelaSocio(codNovo ,nome, cpf, endereco, numeroendereco, estadoCivil, genero);

		TabelaSocioDAO daoSocio = new TabelaSocioDAO();
		if(daoSocio.Alterar(socio, codSocio)){
			JOptionPane.showMessageDialog(null,"Sucesso", "Alterado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao Salvar.",JOptionPane.ERROR_MESSAGE);
		}
	}
	/////////////////////////// deletar //////////////////
	public void DeletarSocio(String codSocio) {
		
		TabelaSocioDAO daoSocio = new TabelaSocioDAO();
		if(daoSocio.Delete(codSocio)){
			JOptionPane.showConfirmDialog(null,
					"<html>Deseja deletar o livro:<br>codSocio.: " + codSocio + "<br>\"" + "\"?</html>");
			JOptionPane.showMessageDialog(null,"Sucesso", "deletado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao deletar.",JOptionPane.ERROR_MESSAGE);
		}}

	///////////////////////////ALTERAR estado ///////////////////////
	public void AlterarEstado(String codSocio,String estado){
		
		ModeloTabelaSocio socio = new ModeloTabelaSocio(codSocio,estado);
		TabelaSocioDAO daoSocio = new TabelaSocioDAO();
		
		if(daoSocio.AlterarEstado(socio, estado,codSocio)){
			JOptionPane.showMessageDialog(null,"Alterado com Sucesso.", "Alterado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro ao alterar.", "Problemas ao Alterar.",JOptionPane.ERROR_MESSAGE);
		}
		
	}
}








