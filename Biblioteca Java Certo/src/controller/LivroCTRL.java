package controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import model.ModeloTabelaLivro;
import model.ModeloTabelaSocio;
import dao.TabelaLivrosDAO; 
import dao.TabelaSocioDAO;

public class LivroCTRL {


	////////////////////////////// cadastrar ////////////////////////////////

	public void CadastrarLivro(String codLivro, String titulo, String autor,int ano) {
		ModeloTabelaLivro livro = new ModeloTabelaLivro( );

		livro.setCodLivro(codLivro);
		livro.setTitulo(titulo);
		livro.setAutor(autor);
		livro.setAno(ano);

		TabelaLivrosDAO daoLivro = new TabelaLivrosDAO();
		//chama DAO
		if(daoLivro.Inserir(livro)){
			JOptionPane.showMessageDialog(null,"Sucesso", "Gravado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao gravar.",JOptionPane.ERROR_MESSAGE);
		}

	}
	/////////////////////////////// deletar /////////////////////////////

	public void DeletarLivro(String codLivro) {

		TabelaLivrosDAO daoLivro = new TabelaLivrosDAO();
		if(daoLivro.Delete(codLivro)){
			JOptionPane.showConfirmDialog(null,
					"<html>Deseja deletar o livro:<br>codLivro.: " + codLivro + "<br>\"" + "\"?</html>");
			JOptionPane.showMessageDialog(null,"Sucesso", "Deletado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao deletar.",JOptionPane.ERROR_MESSAGE);
		}}




	///////////////////// alterar/////////////////////////////
	public void AlterarLivro(String codLivro, String titulo, String autor,
			int ano, String dataIda, String dataVolta) {


		ModeloTabelaLivro livro = new ModeloTabelaLivro(codLivro, titulo, autor, ano,dataIda,dataVolta);

		TabelaLivrosDAO daoLivro = new TabelaLivrosDAO();
		if(daoLivro.Alterar(livro, codLivro)){
			JOptionPane.showMessageDialog(null,"Sucesso", "Alterado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao Salvar.",JOptionPane.ERROR_MESSAGE);
		}
	}

	////////////////////////////alterar estado/////////////////////
	public void AlterarEstado(String codLivro,String estado,String dataIda,String dataVolta,String key){

		ModeloTabelaLivro livro = new ModeloTabelaLivro(codLivro,estado,dataIda,dataVolta);
		TabelaLivrosDAO daoLivro = new TabelaLivrosDAO();

		if(daoLivro.AlterarEstado(livro,codLivro )){
			JOptionPane.showMessageDialog(null,"Alterado com Sucesso.", "Alterado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro ao alterar.", "Problemas ao Alterar.",JOptionPane.ERROR_MESSAGE);
		}

	}

}