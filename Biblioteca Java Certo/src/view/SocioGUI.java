package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import controller.SocioCTRL;

public class SocioGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCpf;
	private JTextField txtEndereco;
	private JTextField txtCodSocio;
	private JTextField txtNumeroendereco;
	private JTextField txtEstadoCivil;
	private JTextField txtCodigoAlterar;
	private JTextField txtNomeAlterar;
	private JTextField txtCpfAlterar;
	private JTextField txtEnderecoAlterar;
	private JTextField txtEstadoCivilAlterar;
	private JTextField txtNumeroenderecoAlterar;
	private JTextField txtCodigoDeletar;
	private JTextField txtCodSocioES;
	private JTextField txtGenero;


	private SocioCTRL controle;
	private JTextField txtGeneroAlterar;
	private JTextField txtEstado;
	private JTextField txtCodigoAlterar2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SocioGUI frame = new SocioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SocioGUI() {
		setTitle("S\u00F3cio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 445, 297);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		controle = new SocioCTRL();


		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 434, 261);
		contentPane.add(tabbedPane);

		JPanel Cadastrar = new JPanel();
		tabbedPane.addTab("Cadastrar", null, Cadastrar, null);
		Cadastrar.setLayout(null);

		JLabel lblNome = new JLabel("Nome:");
		lblNome.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Nome.png")));
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNome.setBounds(10, 30, 62, 25);
		Cadastrar.add(lblNome);

		JLabel lblNewLabel = new JLabel("CPF:");
		lblNewLabel.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Cpf.png")));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(10, 66, 62, 14);
		Cadastrar.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Endere\u00E7o:");
		lblNewLabel_1.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Endere\u00E7o.png")));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(10, 91, 96, 24);
		Cadastrar.add(lblNewLabel_1);

		txtNome = new JTextField();
		txtNome.setBounds(82, 33, 337, 20);
		Cadastrar.add(txtNome);
		txtNome.setColumns(10);

		txtCpf = new JTextField();
		txtCpf.setBounds(63, 64, 129, 20);
		Cadastrar.add(txtCpf);
		txtCpf.setColumns(10);

		txtEndereco = new JTextField();
		txtEndereco.setBounds(92, 94, 221, 20);
		Cadastrar.add(txtEndereco);
		txtEndereco.setColumns(10);

		JLabel lblCdigo = new JLabel("C\u00F3digo:");
		lblCdigo.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Qr Code.png")));
		lblCdigo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCdigo.setBounds(10, 8, 71, 14);
		Cadastrar.add(lblCdigo);

		txtCodSocio = new JTextField();
		txtCodSocio.setBounds(82, 6, 337, 20);
		Cadastrar.add(txtCodSocio);
		txtCodSocio.setColumns(10);

		JLabel lblN = new JLabel("N\u00BA:");
		lblN.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Numero.png")));
		lblN.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblN.setBounds(318, 91, 38, 24);
		Cadastrar.add(lblN);

		txtNumeroendereco = new JTextField();
		txtNumeroendereco.setBounds(366, 94, 56, 20);
		Cadastrar.add(txtNumeroendereco);
		txtNumeroendereco.setColumns(10);

		JLabel lblEstadoCivil = new JLabel("Estado Civil:");
		lblEstadoCivil.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Estado Civil.png")));
		lblEstadoCivil.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEstadoCivil.setBounds(10, 129, 96, 14);
		Cadastrar.add(lblEstadoCivil);

		txtEstadoCivil = new JTextField();
		txtEstadoCivil.setBounds(107, 127, 129, 20);
		Cadastrar.add(txtEstadoCivil);
		txtEstadoCivil.setColumns(10);

		JLabel lblGnero = new JLabel("G\u00EAnero:");
		lblGnero.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Genero.png")));
		lblGnero.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblGnero.setBounds(10, 154, 71, 14);
		Cadastrar.add(lblGnero);

		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				SalvarSocio();

			}});
		btnSalvar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSalvar.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Save-icon.png")));
		btnSalvar.setBounds(138, 197, 139, 25);
		Cadastrar.add(btnSalvar);

		txtGenero = new JTextField();
		txtGenero.setBounds(79, 152, 157, 20);
		Cadastrar.add(txtGenero);
		txtGenero.setColumns(10);

		JPanel Alterar = new JPanel();
		tabbedPane.addTab("Alterar", null, Alterar, null);
		Alterar.setLayout(null);

		JLabel lblNewLabel_2 = new JLabel("C\u00F3digo:");
		lblNewLabel_2.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Qr Code.png")));
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(10, 11, 62, 14);
		Alterar.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Nome:");
		lblNewLabel_3.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Nome.png")));
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_3.setBounds(10, 41, 62, 14);
		Alterar.add(lblNewLabel_3);

		txtCodigoAlterar = new JTextField();
		txtCodigoAlterar.setBounds(82, 9, 112, 20);
		Alterar.add(txtCodigoAlterar);
		txtCodigoAlterar.setColumns(10);

		txtNomeAlterar = new JTextField();
		txtNomeAlterar.setBounds(82, 39, 337, 20);
		Alterar.add(txtNomeAlterar);
		txtNomeAlterar.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("CPF:");
		lblNewLabel_4.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Cpf.png")));
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_4.setBounds(10, 71, 46, 14);
		Alterar.add(lblNewLabel_4);

		txtCpfAlterar = new JTextField();
		txtCpfAlterar.setBounds(66, 69, 128, 20);
		Alterar.add(txtCpfAlterar);
		txtCpfAlterar.setColumns(10);

		JLabel lblEndereo = new JLabel("Endere\u00E7o:");
		lblEndereo.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Endere\u00E7o.png")));
		lblEndereo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEndereo.setBounds(10, 103, 78, 14);
		Alterar.add(lblEndereo);

		txtEnderecoAlterar = new JTextField();
		txtEnderecoAlterar.setBounds(98, 101, 224, 20);
		Alterar.add(txtEnderecoAlterar);
		txtEnderecoAlterar.setColumns(10);

		JLabel lblNewLabel_5 = new JLabel("Estado Civil:");
		lblNewLabel_5.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Estado Civil.png")));
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_5.setBounds(10, 128, 87, 14);
		Alterar.add(lblNewLabel_5);

		txtEstadoCivilAlterar = new JTextField();
		txtEstadoCivilAlterar.setBounds(98, 126, 125, 20);
		Alterar.add(txtEstadoCivilAlterar);
		txtEstadoCivilAlterar.setColumns(10);

		JLabel lblN_1 = new JLabel("N\u00BA");
		lblN_1.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Numero.png")));
		lblN_1.setBounds(325, 104, 41, 14);
		Alterar.add(lblN_1);

		txtNumeroenderecoAlterar = new JTextField();
		txtNumeroenderecoAlterar.setBounds(367, 101, 52, 20);
		Alterar.add(txtNumeroenderecoAlterar);
		txtNumeroenderecoAlterar.setColumns(10);

		JLabel lblNewLabel_6 = new JLabel("G\u00EAnero:");
		lblNewLabel_6.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Genero.png")));
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_6.setBounds(10, 157, 78, 14);
		Alterar.add(lblNewLabel_6);

		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				AlterarSocio();
			}
		});
		btnAlterar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAlterar.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Edit.png")));
		btnAlterar.setBounds(127, 193, 139, 29);
		Alterar.add(btnAlterar);

		txtGeneroAlterar = new JTextField();
		txtGeneroAlterar.setBounds(82, 155, 86, 20);
		Alterar.add(txtGeneroAlterar);
		txtGeneroAlterar.setColumns(10);
		
		JLabel lblCodigoNovo = new JLabel("Codigo Novo:");
		lblCodigoNovo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCodigoNovo.setBounds(204, 12, 87, 13);
		Alterar.add(lblCodigoNovo);
		
		txtCodigoAlterar2 = new JTextField();
		txtCodigoAlterar2.setBounds(301, 9, 86, 20);
		Alterar.add(txtCodigoAlterar2);
		txtCodigoAlterar2.setColumns(10);

		JPanel Deletar = new JPanel();
		tabbedPane.addTab("Deletar", null, Deletar, null);
		Deletar.setLayout(null);

		JLabel lblCodigo = new JLabel("C\u00F3digo:");
		lblCodigo.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Qr Code.png")));
		lblCodigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCodigo.setBounds(10, 11, 75, 18);
		Deletar.add(lblCodigo);

		txtCodigoDeletar = new JTextField();
		txtCodigoDeletar.setBounds(95, 9, 292, 20);
		Deletar.add(txtCodigoDeletar);
		txtCodigoDeletar.setColumns(10);

		JButton btnApagar = new JButton("Deletar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				DeletarSocio();
				
			}
		});
		btnApagar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnApagar.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Delete.png")));
		btnApagar.setBounds(133, 69, 129, 48);
		Deletar.add(btnApagar);

		JPanel MudarES = new JPanel();
		tabbedPane.addTab("Mudar ES", null, MudarES, null);
		MudarES.setLayout(null);

		JButton btnNewButton = new JButton("Salvar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AlterarEstado();
			}
		});
		btnNewButton.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Save-icon.png")));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(121, 173, 182, 49);
		MudarES.add(btnNewButton);
		
		txtCodSocioES = new JTextField();
		txtCodSocioES.setBounds(130, 11, 253, 20);
		MudarES.add(txtCodSocioES);
		txtCodSocioES.setColumns(10);

		JLabel lblCodigoSocioMES = new JLabel("C\u00F3digo do S\u00F3cio:");
		lblCodigoSocioMES.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Qr Code.png")));
		lblCodigoSocioMES.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCodigoSocioMES.setBounds(10, 13, 148, 14);
		MudarES.add(lblCodigoSocioMES);

		JLabel lblIcon = new JLabel("");
		lblIcon.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Divida1.png")));
		lblIcon.setBounds(24, 100, 46, 33);
		MudarES.add(lblIcon);

		JLabel lblIcon2 = new JLabel("");
		lblIcon2.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Em dia1.png")));
		lblIcon2.setBounds(296, 100, 46, 33);
		MudarES.add(lblIcon2);

		JLabel lblEstado = new JLabel("Estado:");
		lblEstado.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEstado.setBounds(140, 69, 59, 20);
		MudarES.add(lblEstado);

		txtEstado = new JTextField();
		txtEstado.setBounds(70, 100, 216, 33);
		MudarES.add(txtEstado);
		txtEstado.setColumns(10);
		
				JPanel Exit = new JPanel();
				tabbedPane.addTab("Exit", null, Exit, null);
				Exit.setLayout(null);
				
						JButton btnNewButton_1 = new JButton("Sair");
						btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
						btnNewButton_1.setIcon(new ImageIcon(SocioGUI.class.getResource("/IMG/Exit1.png")));
						btnNewButton_1.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								dispose();
							}


						});
						btnNewButton_1.setBounds(135, 83, 137, 58);
						Exit.add(btnNewButton_1);
	}

	//////////////metodo para get/////////////
	private void SalvarSocio() {

		String codSocio = txtCodSocio.getText();
		String nome= txtNome.getText();
		String cpf = txtCpf.getText();
		String endereco = txtEndereco.getText();
		int numeroendereco = Integer.parseInt(txtNumeroendereco.getText());
		String estadoCivil = txtEstadoCivil.getText();
		String genero = txtGenero.getText();

		controle.CadastrarSocio(codSocio, nome, cpf, endereco, numeroendereco, estadoCivil, genero);
	}

	private void AlterarSocio(){

		String codSocio = txtCodigoAlterar.getText();
		String codNovo = txtCodigoAlterar2.getText();
		String nome= txtNomeAlterar.getText();
		String cpf = txtCpfAlterar.getText();
		String endereco = txtEnderecoAlterar.getText();
		int numeroendereco = Integer.parseInt(txtNumeroenderecoAlterar.getText());
		String estadoCivil = txtEstadoCivilAlterar.getText();
		String genero = txtGeneroAlterar.getText();

		controle.AlterarSocio(codSocio, nome, cpf, endereco, numeroendereco, estadoCivil, genero,codNovo);
	}
	
	private void DeletarSocio(){
		String codSocio = txtCodigoDeletar.getText();
	    controle.DeletarSocio(codSocio);
	    
	}
	
	private void AlterarEstado(){
		String codSocio = txtCodSocioES.getText();
		String estado = txtEstado.getText();
		controle.AlterarEstado(codSocio, estado);
	}


}
