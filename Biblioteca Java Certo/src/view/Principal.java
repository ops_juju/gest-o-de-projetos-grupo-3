package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JLabel;

import java.awt.Panel;
//RataLana@bitbucket.org/ops_juju/gest-o-de-projetos-grupo-3.git

//anova funcionalidade que vamos desevnolver aqui com dislequicia 
public class Principal extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Principal.class.getResource("/IMG/Livro.png")));
		setTitle("Principal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 399, 439);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Principal.class.getResource("/IMG/Gif.gif")));
		lblNewLabel.setBounds(0, 0, 383, 379);
		getContentPane().add(lblNewLabel);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnPessoas = new JMenu("Arquivo");
		mnPessoas.setIcon(new ImageIcon(Principal.class.getResource("/IMG/Endere\u00E7o.png")));
		menuBar.add(mnPessoas);
		
		JMenuItem mntmSocio = new JMenuItem("S\u00F3cio");
		mntmSocio.setIcon(new ImageIcon(Principal.class.getResource("/IMG/Person-Female-Light-icon.png")));
		mntmSocio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				SocioGUI socio = new SocioGUI();
				socio.setVisible(true);
			}
		});
		mnPessoas.add(mntmSocio);
		
		JMenuItem mntmLivro = new JMenuItem("Livro");
		mntmLivro.setIcon(new ImageIcon(Principal.class.getResource("/IMG/Livro.png")));
		mntmLivro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				LivroGUI livro = new LivroGUI();
				livro.setVisible(true);
			}
		});
		mnPessoas.add(mntmLivro);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mntmSair.setIcon(new ImageIcon(Principal.class.getResource("/IMG/Exit.png")));
		mnPessoas.add(mntmSair);
	}
}
