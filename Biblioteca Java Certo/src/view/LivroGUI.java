package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import controller.LivroCTRL;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class LivroGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtTitulo;
	private JTextField txtCodLivro;
	private JTextField txtCodigoDeletar;
	private JTextField txtAutor;
	private JTextField txtCodLivroES;
	private JTextField txtDataIda;
	private JTextField txtDataVolta;
	private JTextField txtEstado;
	private JTextField txtAno;
	LivroCTRL controle = new LivroCTRL();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LivroGUI frame = new LivroGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LivroGUI() {
		setTitle("Livro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 407, 283);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 381, 244);
		contentPane.add(tabbedPane);

		JPanel Cadastrar = new JPanel();
		tabbedPane.addTab("Cadastrar", null, Cadastrar, null);
		Cadastrar.setLayout(null);

		JLabel lblNome = new JLabel("Titulo:");
		lblNome.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Titulo.png")));
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNome.setBounds(7, 47, 67, 25);
		Cadastrar.add(lblNome);

		txtTitulo = new JTextField();
		txtTitulo.setBounds(74, 50, 199, 20);
		Cadastrar.add(txtTitulo);
		txtTitulo.setColumns(10);

		JLabel lblCdigoCadastrar = new JLabel("C\u00F3digo:");
		lblCdigoCadastrar.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Qr Code.png")));
		lblCdigoCadastrar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCdigoCadastrar.setBounds(7, 22, 70, 14);
		Cadastrar.add(lblCdigoCadastrar);

		txtCodLivro = new JTextField();
		txtCodLivro.setBounds(74, 19, 199, 20);
		Cadastrar.add(txtCodLivro);
		txtCodLivro.setColumns(10);

		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Save-icon.png")));
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Cadastrar();
			}
		});
		btnCadastrar.setBounds(88, 169, 139, 25);
		Cadastrar.add(btnCadastrar);

		JLabel lblAutor = new JLabel("Autor:");
		lblAutor.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Autor1.png")));
		lblAutor.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblAutor.setBounds(7, 84, 67, 25);
		Cadastrar.add(lblAutor);

		txtAutor = new JTextField();
		txtAutor.setBounds(74, 87, 199, 20);
		Cadastrar.add(txtAutor);
		txtAutor.setColumns(10);

		JLabel lblAno = new JLabel("Ano:");
		lblAno.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/date_icon.png")));
		lblAno.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblAno.setBounds(7, 122, 67, 20);
		Cadastrar.add(lblAno);

		txtAno = new JTextField();
		txtAno.setBounds(72, 120, 201, 20);
		Cadastrar.add(txtAno);
		txtAno.setColumns(10);

		JPanel Deletar = new JPanel();
		tabbedPane.addTab("Deletar", null, Deletar, null);
		Deletar.setLayout(null);

		JLabel lblCodigoDeletar = new JLabel("C\u00F3digo:");
		lblCodigoDeletar.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Qr Code.png")));
		lblCodigoDeletar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCodigoDeletar.setBounds(10, 8, 86, 18);
		Deletar.add(lblCodigoDeletar);

		txtCodigoDeletar = new JTextField();
		txtCodigoDeletar.setBounds(96, 9, 185, 20);
		Deletar.add(txtCodigoDeletar);
		txtCodigoDeletar.setColumns(10);

		JButton btnDeletar = new JButton("Deletar");
		btnDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Deletar();
			}
		});
		btnDeletar.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Delete.png")));
		btnDeletar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnDeletar.setBounds(91, 61, 114, 47);
		Deletar.add(btnDeletar);

		JPanel Mudar = new JPanel();
		tabbedPane.addTab("Mudar ES", null, Mudar, null);
		Mudar.setLayout(null);

		JButton btnSalvar_1 = new JButton("Salvar");
		btnSalvar_1.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Save-icon.png")));
		btnSalvar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				AlterarEstadoLivro(); 
				
			}
		});
		btnSalvar_1.setBounds(10, 163, 295, 23);
		Mudar.add(btnSalvar_1);

		txtCodLivroES = new JTextField();
		txtCodLivroES.setBounds(107, 11, 198, 20);
		Mudar.add(txtCodLivroES);
		txtCodLivroES.setColumns(10);

		JLabel lblCdigoScio = new JLabel("C\u00F3digo Livro:");
		lblCdigoScio.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Qr Code.png")));
		lblCdigoScio.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCdigoScio.setBounds(10, 13, 94, 14);
		Mudar.add(lblCdigoScio);

		JLabel lblDataIda = new JLabel("Data Ida:");
		lblDataIda.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/date_icon.png")));
		lblDataIda.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDataIda.setBounds(10, 44, 89, 18);
		Mudar.add(lblDataIda);

		txtDataIda = new JTextField();
		txtDataIda.setBounds(107, 44, 198, 20);
		Mudar.add(txtDataIda);
		txtDataIda.setColumns(10);

		JLabel lblDataVolta = new JLabel("Data Volta:");
		lblDataVolta.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/date_icon.png")));
		lblDataVolta.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDataVolta.setBounds(10, 81, 89, 23);
		Mudar.add(lblDataVolta);

		txtDataVolta = new JTextField();
		txtDataVolta.setBounds(107, 83, 198, 20);
		Mudar.add(txtDataVolta);
		txtDataVolta.setColumns(10);

		txtEstado = new JTextField();
		txtEstado.setBounds(107, 117, 198, 20);
		Mudar.add(txtEstado);
		txtEstado.setColumns(10);

		JLabel lblNewLabel = new JLabel("Estado:");
		lblNewLabel.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Em dia1.png")));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(10, 115, 69, 23);
		Mudar.add(lblNewLabel);

		JPanel Exit = new JPanel();
		tabbedPane.addTab("Exit", null, Exit, null);
		Exit.setLayout(null);

		JButton btnSair = new JButton("Sair");
		btnSair.setIcon(new ImageIcon(LivroGUI.class.getResource("/IMG/Exit1.png")));
		btnSair.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSair.setBounds(52, 35, 202, 64);
		Exit.add(btnSair);
	}
	private void Cadastrar() {

		String codigo = txtCodLivro.getText();
		String titulo = txtTitulo.getText();
		String autor = txtAutor.getText();
		int ano = Integer.parseInt(txtAno.getText());

		controle.CadastrarLivro(codigo, titulo,autor,ano);
	}



	private void Deletar() {
		String codigo = txtCodigoDeletar.getText();

		controle.DeletarLivro(codigo);
	}
	
	private void AlterarEstadoLivro(){
		String codLivro = txtCodLivroES.getText();
		String estado = txtEstado.getText();
		String DataIda = txtDataIda.getText();
		String DataVolta= txtDataVolta.getText();

		controle.AlterarEstado(codLivro, estado,DataIda,DataVolta,codLivro);
	}
}
